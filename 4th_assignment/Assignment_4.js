

data =
'[{"name" : "Ashwin Sharma", "designation" : "Trainee", "technology" : "AI", "experience" :"8 Weeks" , "profile" : "https://media.istockphoto.com/photos/positivity-produces-success-picture-id1132793417?k=6&m=1132793417&s=612x612&w=0&h=-v-zYomA2sFfE_UP2eRjWqgk6PAfitVktECQDZn-y4o="} , {"name" : "Rahul Jain", "designation" : "Trainee", "technology" : "ReactJs", "experience" : "5 Weeks", "profile" : "https://image.freepik.com/free-photo/close-up-portrait-handsome-young-dark-skinned-student-employee_273609-9115.jpg"}, {"name" : "Vikas Gupta", "designation" : "Trainee", "technology" : "Angular", "experience" : "3 Weeks", "profile" : "https://image.shutterstock.com/image-photo/portrait-smiling-man-office-video-260nw-1854580573.jpg"}, {"name" : "Aakash Jain", "designation" : "Trainee", "technology" : "Cyber Security", "experience" : "4 Weeks", "profile" : "https://media.istockphoto.com/photos/m-here-to-make-it-happen-picture-id968943486?k=6&m=968943486&s=612x612&w=0&h=3iL0OIBOs-WeFn8mid0tiMRUT0hWn5bvBchY1NJPI1A="}, {"name" : "Ashwin Sharma", "designation" : "Trainee", "technology" : "IoT", "experience" : "6 Weeks", "profile" : "https://www.viennashots.com/wp-content/uploads/2019/05/Mitarbeiterportraits_Wien_Detail-58-e1559232158493.jpg"}]';
var ind = -1;

function generateCurrentEmployee(obj) {
  var generated_emp = "";
  generated_emp += "<h4>Name : " + obj.name + "</h4>" ;
  
  generated_emp += "<h4>Designation : " + obj.designation + "</h4>";
  generated_emp += "<h4>Technology : " + obj.technology + "</h4>";
  generated_emp += "<h4>Experience : " + obj.experience + "</h4>";
  return generated_emp;
}

function Next() {
  ind += 1;
  var obj = JSON.parse(data);
  console.log(obj[ind]);
  var generated_emp = generateCurrentEmployee(obj[ind]);
  document.getElementById("emp_image").src = obj[ind].profile;
  document.getElementById("emp_details").innerHTML = generated_emp;
}

function Prev() {
  ind -= 1;
  var obj = JSON.parse(data);
  console.log(obj[ind]);
  var generated_emp = generateCurrentEmployee(obj[ind]);
  document.getElementById("emp_image").src = obj[ind].profile;
  document.getElementById("emp_details").innerHTML = generated_emp;
}

// function validateForm() {
 
//   var obj = JSON.parse(data);
//   document.getElementById("emp_image").src = obj[0].profile;
//   console.log(obj);
// }
